package com.netlink.logParser;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

import java.time.*;
import java.time.format.DateTimeFormatter;

class LogParserApplicationTests {

	@Test
	void contextLoads() {

		String line ="02 Nov 2020 15:55:40,888 | INFO  | Camel (connector-for-sanctions) thread #4 - saa://read(CFS_Screen) BIC=AAAAAAAA MID=205FE89BFFA43F786E37E4D1D357DFAF2508E151 | screenInputMessages | 217 - org.apache.camel.camel-core - 2.17.0.redhat-630254 | Message [UUMID=ISABRRUMMXXX103FT2030740008] was identified as suspect with message ID [MID=205FE89BFFA43F786E37E4D1D357DFAF2508E151] (in exchange ID-cebnlsaa1-60829-1604328891962-0-6).  Messages disposed to queue CFS_Suspect.";
		String string = StringUtils.substringBetween(line, "[UUMID=", "]");
		String date = StringUtils.substring(line,0,line.indexOf(","));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");
		LocalDateTime localDate = LocalDateTime.parse(date.trim(), formatter);
		Instant instant = localDate.toInstant(ZoneOffset.UTC);
		String bic = StringUtils.substring(string,1, 12);
		String messageType = StringUtils.substring(string, 12, 15);
		String messageRef = StringUtils.substring(string, 15);
		System.out.println(string);
		System.out.println(date);
		System.out.println(bic);
		System.out.println(messageType);
		System.out.println(messageRef);
	    System.out.println(instant.toString());
	}

}
