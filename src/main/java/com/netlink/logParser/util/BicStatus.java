package com.netlink.logParser.util;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum BicStatus {
    PEND("Pending"),
    REJECT("Rejected"),
    NOTVALIDATED("NotValidated"),
    OK("Verified");

    public final String label;
}
