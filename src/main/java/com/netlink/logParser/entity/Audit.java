package com.netlink.logParser.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@ToString
@Getter
@Setter
@Document(collection = "Audit")
public class Audit {
    @Id
    private String id;
    private String username;
    private String bic;
    private String messageType;
    private String messageRef;
    private Instant actionDate;
    private Boolean state;

}
