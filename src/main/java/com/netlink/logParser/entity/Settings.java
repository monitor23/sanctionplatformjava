package com.netlink.logParser.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@ToString
@Getter
@Setter
@Document(collection = "settings")
public class Settings {
    @Id
    private String id;
    private Boolean enableBicValidation;
}
