package com.netlink.logParser.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
@ToString
@Getter
@Setter
@Document(collection = "LogInfo")
public class LogInfo {
    @Id
    private String id;
    private boolean processed;
    private String payload;
    private String bic;
    private String messageType;
    private String messageRef;
    private Instant createdDate;
    private boolean checked;
    private String hashCode;
    private String status;
}
