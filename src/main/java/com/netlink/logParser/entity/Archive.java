package com.netlink.logParser.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@ToString
@Getter
@Setter
@Document(collection = "Archive")
public class Archive {
    @Id
    private String id;
    private Instant createdDate;
    private Instant fromDate;
    private Instant untilDate;
    private Binary payloadFile;
}
