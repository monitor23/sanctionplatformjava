package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class ValidatorResponse implements Serializable {
    private boolean valid;
    private String message;
}
