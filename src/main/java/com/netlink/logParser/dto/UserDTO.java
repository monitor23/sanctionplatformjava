package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class UserDTO {
    private String id;
    private String username;
    private String lastName;
    private String firstName;
    private String email;
    private String password;
}
