package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoughnutDTO {

    private int verifiedCount;
    private int notVerifiedCount;
}
