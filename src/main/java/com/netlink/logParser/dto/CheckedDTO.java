package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckedDTO {
    String id;
}
