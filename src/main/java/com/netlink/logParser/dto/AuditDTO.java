package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class AuditDTO {

    private String id;
    private String username;
    private String bic;
    private String messageType;
    private String messageRef;
    private Instant actionDate;
    private String state;
}
