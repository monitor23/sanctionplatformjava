package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SettingsDTO {
    private String id;
    private Boolean enableBicValidation;
}
