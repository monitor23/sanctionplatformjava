package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class PayloadDTO {
    private String bic;
    private String messageType;
    private String messageRef;
    private Instant time;
}
