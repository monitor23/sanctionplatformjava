package com.netlink.logParser.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class TableDTO {
    private String id;
    private String bic;
    private String messageType;
    private String messageRef;
    private Instant createdDate;
    private boolean checked;
    private String bicStatus;
}
