package com.netlink.logParser.repository;

import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.entity.Settings;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;

public interface SettingsRepository extends CrudRepository<Settings, String> {
}
