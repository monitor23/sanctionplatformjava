package com.netlink.logParser.repository;

import com.netlink.logParser.entity.LogInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;

public interface LogInfoRepository extends CrudRepository<LogInfo, String> {
    public List<LogInfo> findByProcessedOrderByCreatedDateDesc(boolean processed);
    public List<LogInfo> findByChecked(boolean checked);
    public LogInfo findByHashCode(String hashCode);
    public List<LogInfo> findByStatus(String status);
    public List<LogInfo> findByCreatedDateBetween(Instant start, Instant end);
}
