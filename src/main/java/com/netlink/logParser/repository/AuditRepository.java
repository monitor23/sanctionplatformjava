package com.netlink.logParser.repository;

import com.netlink.logParser.entity.Audit;
import org.springframework.data.repository.CrudRepository;

public interface AuditRepository extends CrudRepository<Audit, String> {
}
