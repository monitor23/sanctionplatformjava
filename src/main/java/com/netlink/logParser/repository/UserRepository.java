package com.netlink.logParser.repository;

import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {

     User findByUsername(String username);
}
