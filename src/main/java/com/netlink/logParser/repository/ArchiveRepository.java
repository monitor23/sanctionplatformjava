package com.netlink.logParser.repository;

import com.netlink.logParser.entity.Archive;
import org.springframework.data.repository.CrudRepository;

public interface ArchiveRepository extends CrudRepository<Archive, String> {
}
