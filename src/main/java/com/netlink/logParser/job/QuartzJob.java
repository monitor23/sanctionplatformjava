package com.netlink.logParser.job;

import com.netlink.logParser.dto.ValidatorResponse;
import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.service.FileService;
import com.netlink.logParser.service.LogService;
import com.netlink.logParser.service.SettingsService;
import com.netlink.logParser.service.ValidationService;
import com.netlink.logParser.util.BicStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

@Slf4j
@Component
public class QuartzJob {
    private File inputFilesPath;
    private LogService logService;
    private FileService fileService;
    private ValidationService validationService;
    private SettingsService settingsService;

    private static final Logger logger = LogManager.getLogger(QuartzJob.class);

    public QuartzJob(Environment env, LogService logService, FileService fileService, ValidationService validationService, SettingsService settingsService) throws IOException {
        this.inputFilesPath = new File(env.getProperty("input.files.path"));
        this.logService = logService;
        this.fileService = fileService;
        this.validationService = validationService;
        this.settingsService = settingsService;
    }

    @Scheduled(fixedDelay = 10000)
    public void extractLogInfo() throws IOException {
        File[] logs = inputFilesPath.listFiles(new FilenameFilter() {
            public boolean accept(File inputFilesPath, String name) {
                return name.endsWith(".log");
            }
        });

        if(logs!=null) {
            for (File logFile : logs) {
                InputStream stream = new FileInputStream(logFile);
                fileService.processFile(stream);
                fileService.moveFile(logFile);
            }
        }
    }

    @Scheduled(fixedDelay = 10000)
    public void processLogInfo() {
        logService.processLogInfo();
    }

    @Scheduled(fixedDelay = 1000)
    public void validatePendingBic() {
        if (settingsService.getBicValidationStatus()) {
            List<LogInfo> logInfoList = logService.findPendingLogs();
            for (LogInfo logInfo : logInfoList) {
                try {
                    ValidatorResponse validatorResponse = validationService.validate(logInfo);
                    if (validatorResponse.isValid()) {
                        logInfo.setStatus(BicStatus.OK.label);
                    } else {
                        logInfo.setStatus(BicStatus.REJECT.label);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage());
                } finally {
                    logService.update(logInfo);
                }
            }
        }
    }
}
