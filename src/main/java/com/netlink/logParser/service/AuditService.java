package com.netlink.logParser.service;

import com.netlink.logParser.dto.AuditDTO;
import com.netlink.logParser.entity.Audit;
import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.repository.AuditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AuditService {
    private AuditRepository auditRepository;

    public void save(LogInfo info, String username) {

        Audit audit = new Audit();
        audit.setUsername(username);
        audit.setBic(info.getBic());
        audit.setMessageRef(info.getMessageRef());
        audit.setMessageType(info.getMessageType());
        audit.setState(info.isChecked());
        audit.setActionDate(Instant.now());
        auditRepository.save(audit);
    }

    public List<AuditDTO> getAll() {
        List<Audit> auditList = (List<Audit>) auditRepository.findAll();
        List<AuditDTO> auditDTOList = new ArrayList<>();
        for(Audit audit: auditList){
            AuditDTO auditDTO = new AuditDTO();
            auditDTO.setUsername(audit.getUsername());
            auditDTO.setBic(audit.getBic());
            auditDTO.setActionDate(audit.getActionDate());
            auditDTO.setMessageRef(audit.getMessageRef());
            auditDTO.setMessageType(audit.getMessageType());
            if(audit.getState()){
                auditDTO.setState("Verified");
            }
            else{
                auditDTO.setState("Not verified");
            }
            auditDTOList.add(auditDTO);
        }
        return auditDTOList;
    }
}
