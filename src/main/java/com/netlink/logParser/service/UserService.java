package com.netlink.logParser.service;

import com.netlink.logParser.dto.TableDTO;
import com.netlink.logParser.dto.UserDTO;
import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.entity.User;
import com.netlink.logParser.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public List<UserDTO> getAll() {
        List<User> users= (List<User>) userRepository.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for(User user: users){
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setUsername(user.getUsername());
            userDTO.setEmail(user.getEmail());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setLastName(user.getLastName());
            userDTOList.add(userDTO);
        }


        return userDTOList;
    }

    public void addUser(UserDTO userDTO) throws Exception {
        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setId(userDTO.getId());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        if(userRepository.findByUsername(user.getUsername())!= null && StringUtils.isEmpty(userDTO.getId())) {
            throw new Exception("Username already exists");
        }
        userRepository.save(user);
    }

    public void deleteById(String id) {
        userRepository.deleteById(id);
    }
}
