package com.netlink.logParser.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.netlink.logParser.dto.ValidatorResponse;
import com.netlink.logParser.entity.LogInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ValidationService {
    private static final Logger logger = LogManager.getLogger(ValidationService.class);
    private String httpBicValidator;
    private String user;
    private String password;
    private String userToken;
    private String passwordToken;
    private String tokenPath;
    private String bicPath;

    public ValidationService(Environment environment) {
        this.httpBicValidator = environment.getProperty("swift.url");
        this.user = environment.getProperty("swift.user");
        this.password = environment.getProperty("swift.password");
        this.userToken = environment.getProperty("swift.token.user");
        this.passwordToken = environment.getProperty("swift.token.pass");
        this.tokenPath = environment.getProperty("swift.token.path");
        this.bicPath = environment.getProperty("swift.bic.path");
    }

    public String getAccessToken() {
        HttpResponse<String> responseU = null;
        HttpResponse<JsonNode> jsonResponse = null;
        String credentials = userToken + ":" + passwordToken;
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + encodedCredentials);
        headers.put("content-type", "application/x-www-form-urlencoded");
        try {
            jsonResponse = Unirest.post(httpBicValidator + tokenPath)
                    .headers(headers)
                    .body("grant_type=password&username=" + user + "&password=" + password)
                    .asJson();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonResponse.getBody().getObject().getString("access_token");
    }

    public ValidatorResponse validate(LogInfo logInfo) {
        ValidatorResponse validatorResponse = new ValidatorResponse();
        validatorResponse.setValid(true);
        validate(validatorResponse, logInfo.getBic());
        return validatorResponse;
    }

    private void validate(ValidatorResponse validatorResponse, String bic) {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {

        }
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + getAccessToken());
        HttpEntity entity = new HttpEntity(headers);
        try {
            ResponseEntity<Object> response = restTemplate.exchange(httpBicValidator + bicPath + bic, HttpMethod.GET, entity, Object.class);
        } catch (HttpClientErrorException e) {
            validatorResponse.setValid(false);
            if (StringUtils.isEmpty(validatorResponse.getMessage())) {
                validatorResponse.setMessage(e.getStatusText() + ": ");
            } else
                validatorResponse.setMessage(validatorResponse.getMessage() + bic + " ");
        }
    }
}
