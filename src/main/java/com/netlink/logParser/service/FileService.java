package com.netlink.logParser.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@Service
public class FileService {
    private LogService logService;
    private String processedFilesPath;

    public FileService(Environment env, LogService logService) {
        this.logService = logService;
        this.processedFilesPath = env.getProperty("processed.files.path");
    }

    public void processFile(InputStream stream) throws IOException {
        InputStreamReader isReader = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isReader);
        String line;

        while ((line = reader.readLine()) != null) {
            if (line.contains("was identified as suspect with message ID")) {
                logService.addSuspectLineToDb(line);
                log.info(line);
            }
        }
        stream.close();
        isReader.close();
        reader.close();
    }

    public void moveFile(File from) throws IOException {
        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        String newFileName = from.getName().replaceAll("[.][^.]+$", fileSuffix + ".log");
        Path src = Paths.get(from.getAbsolutePath());
        Path dest = Paths.get(processedFilesPath + newFileName);
        Path temp = Files.move(src, dest, REPLACE_EXISTING);
        if (temp != null) {
            log.info("File was moved to " + dest.toString());
        } else {
            log.error("Failed to move the file");
        }
    }

    public void processArchiveFile(File file){


    }
}
