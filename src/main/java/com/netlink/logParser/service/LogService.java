package com.netlink.logParser.service;

import com.netlink.logParser.dto.DoughnutDTO;
import com.netlink.logParser.dto.PayloadDTO;
import com.netlink.logParser.dto.TableDTO;
import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.repository.LogInfoRepository;
import com.netlink.logParser.util.BicStatus;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class LogService {
    private LogInfoRepository logInfoRepository;
    private AuditService auditService;

    public void addSuspectLineToDb(String line){
        LogInfo logInfo = new LogInfo();
        logInfo.setPayload(line);
        logInfo.setHashCode(String.valueOf(line.hashCode()));
        if(logInfoRepository.findByHashCode(logInfo.getHashCode()) == null) {
            logInfoRepository.save(logInfo);
        }
    }

    public void processLogInfo() {
        List<LogInfo> logInfoList = logInfoRepository.findByProcessedOrderByCreatedDateDesc(false);
        for(LogInfo logInfo: logInfoList){
            String payload = logInfo.getPayload();
            if (StringUtils.isNotEmpty(payload)){
                PayloadDTO dto = processPayload(payload);
                logInfo.setBic(dto.getBic());
                logInfo.setMessageType(dto.getMessageType());
                logInfo.setMessageRef(dto.getMessageRef());
                logInfo.setCreatedDate(dto.getTime());
                logInfo.setStatus(BicStatus.PEND.label);
            }
            logInfo.setProcessed(true);
            logInfoRepository.save(logInfo);

        }

    }

    public PayloadDTO processPayload(String payload){

        PayloadDTO dto = new PayloadDTO();
        String string = StringUtils.substringBetween(payload, "[UUMID=", "]");
        String date = StringUtils.substring(payload,0,payload.indexOf("|"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss,SSS");
        LocalDateTime localDate = LocalDateTime.parse(date.trim(), formatter);
        Instant instant = localDate.toInstant(ZoneOffset.UTC);
        dto.setTime(instant);
        String bic = StringUtils.substring(string,1, 12);
        dto.setBic(bic);
        String messageType = StringUtils.substring(string, 12, 15);
        dto.setMessageType(messageType);
        String messageRef = StringUtils.substring(string, 15);
        dto.setMessageRef(messageRef);
        return dto;
    }

    public List<TableDTO> getAll() {
        List<LogInfo> logInfoList = logInfoRepository.findByProcessedOrderByCreatedDateDesc(true);
        List<TableDTO> dtoList = new ArrayList<>();

        for( LogInfo logInfo : logInfoList){
            TableDTO tableDTO = new TableDTO();
            tableDTO.setBic(logInfo.getBic());
            tableDTO.setCreatedDate(logInfo.getCreatedDate());
            tableDTO.setChecked(logInfo.isChecked());
            tableDTO.setMessageRef(logInfo.getMessageRef());
            tableDTO.setMessageType(logInfo.getMessageType());
            tableDTO.setId(logInfo.getId());
            tableDTO.setBicStatus(logInfo.getStatus());
            dtoList.add(tableDTO);
        }
        return dtoList;
    }

    public void markLogInfo(String id, String username) {
        Optional<LogInfo> logInfo = logInfoRepository.findById(id);
        if (logInfo.isPresent()) {
            LogInfo info = logInfo.get();
            info.setChecked(!info.isChecked());
            logInfoRepository.save(info);
            auditService.save(info, username);
        }
    }

    public DoughnutDTO getDoughnutData() {
        DoughnutDTO doughnutDTO = new DoughnutDTO();
        doughnutDTO.setVerifiedCount(logInfoRepository.findByChecked(true).size());
        doughnutDTO.setNotVerifiedCount(logInfoRepository.findByChecked(false).size());//findAll().size()-doughnutDTO.getVerifiedCount());
        return doughnutDTO;
    }

    public List<LogInfo> findPendingLogs(){
        return logInfoRepository.findByStatus(BicStatus.PEND.label);
    }

    public void update(LogInfo logInfo) {
        logInfoRepository.save(logInfo);
    }
}

