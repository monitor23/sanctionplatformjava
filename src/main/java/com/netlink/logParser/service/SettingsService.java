package com.netlink.logParser.service;

import com.netlink.logParser.dto.SettingsDTO;
import com.netlink.logParser.entity.Archive;
import com.netlink.logParser.entity.LogInfo;
import com.netlink.logParser.entity.Settings;
import com.netlink.logParser.repository.ArchiveRepository;
import com.netlink.logParser.repository.LogInfoRepository;
import com.netlink.logParser.repository.SettingsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class SettingsService {

    private SettingsRepository settingsRepository;
    private LogInfoRepository logInfoRepository;
    private ArchiveRepository archiveRepository;
    private Environment env;

    @PostConstruct
    public void initDatabase(){
        List<Settings> settings = (List<Settings>) settingsRepository.findAll();
        if (settings.isEmpty()){
            Settings setts = new Settings();
            setts.setEnableBicValidation(false);
            settingsRepository.save(setts);
        }
    }

    public void changeBicValidation(SettingsDTO settingsDTO) {
        Optional<Settings> settings = settingsRepository.findById(settingsDTO.getId());
        if (settings.isPresent()) {
            Settings sets = settings.get();
            sets.setEnableBicValidation(settingsDTO.getEnableBicValidation());
            settingsRepository.save(sets);
        }
    }

    public boolean getBicValidationStatus() {
        List<Settings> settingsList = (List<Settings>) settingsRepository.findAll();
        if (!settingsList.isEmpty()) {
            Settings settings = settingsList.get(settingsList.size() - 1);
            return settings.getEnableBicValidation();
        } else return false;
    }

    public SettingsDTO getSettings() {
        List<Settings> settings = (List<Settings>) settingsRepository.findAll();
        SettingsDTO settingsDTO = new SettingsDTO();
        if (!settings.isEmpty()) {
            Settings sett = settings.get(settings.size() - 1);
            settingsDTO.setEnableBicValidation(sett.getEnableBicValidation());
            settingsDTO.setId(sett.getId());
        }

        return settingsDTO;
    }

    public void archiveLogs(Instant startInstant, Instant endInstant, String username) throws IOException {
        List<LogInfo> logInfoList = logInfoRepository.findByCreatedDateBetween(startInstant, endInstant);
        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");
        LocalDateTime localDate = LocalDateTime.parse(new Date().toString().trim(), formatter);
        Instant instant = localDate.toInstant(ZoneOffset.UTC);*/
        String filePathname= env.getProperty("archive.files.path")+"archive_"+DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss").format(LocalDateTime.now())+".txt";
       // +DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss").format(LocalDateTime.now())+
        File file = new File(filePathname);
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file);

        for( LogInfo logInfo: logInfoList) {
            byte[] strToBytes = (logInfo.getPayload()+"\n").getBytes();
            outputStream.write(strToBytes);
        }
        Archive archive = new Archive();
        archive.setCreatedDate(Instant.now());
        archive.setFromDate(startInstant);
        archive.setUntilDate(endInstant);
        Binary binary= new Binary(BsonBinarySubType.BINARY, Files.readAllBytes(Paths.get(filePathname)));
        archive.setPayloadFile(binary);
        archiveRepository.save(archive);
        outputStream.close();
        logInfoRepository.deleteAll(logInfoList);

    }
}
