package com.netlink.logParser.controller;

import com.netlink.logParser.dto.SettingsDTO;
import com.netlink.logParser.service.SettingsService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class SettingsController {
    private SettingsService settingsService;

    @PostMapping(value = "/changeBicValidationService", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity changeBicValidationService(
            @RequestBody SettingsDTO settingsDTO) {
        settingsService.changeBicValidation(settingsDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/getSettings", produces = MediaType.APPLICATION_JSON_VALUE)
    public SettingsDTO getSettings() {
        return settingsService.getSettings();
    }

    @GetMapping(value = "/archive", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity archive(@RequestParam String start, @RequestParam String end, HttpServletRequest httpServletRequest) throws IOException {
        LocalDate sdate = LocalDate.parse(start);
        LocalDate edate = LocalDate.parse(end);
        Instant startInstant = sdate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        Instant endInstant = edate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        if (startInstant.equals(endInstant)) {
            endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        }

        settingsService.archiveLogs(startInstant, endInstant, httpServletRequest.getUserPrincipal().getName());
        return new ResponseEntity( HttpStatus.OK);
    }

}
