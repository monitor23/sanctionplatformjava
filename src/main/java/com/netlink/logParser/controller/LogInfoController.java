package com.netlink.logParser.controller;

import com.netlink.logParser.dto.CheckedDTO;
import com.netlink.logParser.dto.TableDTO;
import com.netlink.logParser.service.FileService;
import com.netlink.logParser.service.LogService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class LogInfoController {
    private LogService logService;
    private FileService fileService;

    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TableDTO> getAll() {
        return logService.getAll();
    }

    @PostMapping(value = "/mark", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity postResponseJsonContent(
            @RequestBody CheckedDTO checkedDTO, HttpServletRequest httpServletRequest){
        logService.markLogInfo(checkedDTO.getId(), httpServletRequest.getUserPrincipal().getName());
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/uploadLogFile", produces = MediaType.APPLICATION_JSON_VALUE)
    public void handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        fileService.processFile(file.getInputStream());
    }

}
