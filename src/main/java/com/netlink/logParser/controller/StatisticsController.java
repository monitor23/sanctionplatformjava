package com.netlink.logParser.controller;

import com.netlink.logParser.dto.DoughnutDTO;
import com.netlink.logParser.service.LogService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class StatisticsController {
    private LogService logService;

    @GetMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    public DoughnutDTO getDoughnutData() {
        return logService.getDoughnutData();
    }


}
