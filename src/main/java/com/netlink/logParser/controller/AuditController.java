package com.netlink.logParser.controller;

import com.netlink.logParser.dto.AuditDTO;
import com.netlink.logParser.service.AuditService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class AuditController {
    private AuditService auditService;

    @GetMapping(value = "/getAuditInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuditDTO> getAuditInfo() {
        return auditService.getAll();
    }
}
