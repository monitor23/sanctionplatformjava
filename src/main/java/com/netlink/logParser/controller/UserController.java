package com.netlink.logParser.controller;

import com.netlink.logParser.dto.ResponseDTO;
import com.netlink.logParser.dto.TableDTO;
import com.netlink.logParser.dto.UserDTO;
import com.netlink.logParser.entity.User;
import com.netlink.logParser.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class UserController {
    private UserService userService;

    @GetMapping(value = "/getUsers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDTO> getAll() {
        return userService.getAll();
    }

    @PostMapping(value = "/addUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> addUser(@RequestBody UserDTO userDTO) throws IOException {
        try {
            userService.addUser(userDTO);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteUser/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> deleteUser(@PathVariable("id") String id) throws IOException {
        try {
            userService.deleteById(id);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }

    @GetMapping(value = "/validateLogin", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO validateLogin() {
        return new UserDTO();
    }
}
