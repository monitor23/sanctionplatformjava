package com.netlink.logParser;

import com.netlink.logParser.entity.LogInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class LogParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogParserApplication.class, args);
	}

}
